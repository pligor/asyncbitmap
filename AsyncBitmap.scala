package com.pligor.asyncbitmap

import android.widget.ImageView

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object AsyncBitmap {
  /**
   * @return false if the same work is already taking place. true if we have to create a new task
   */
  def cancelPotentialWork[T, P](imageView: ImageView, bitmapSource: BitmapSource[T, P]): Boolean = {
    val taskOption = grabBitmapWorkerTask(imageView)

    if (taskOption.isDefined) {
      val task = taskOption.get

      val curBitmapSourceOption = task.getCurBitmapSourceOption

      if (curBitmapSourceOption.isDefined) {
        val curBitmapSource = curBitmapSourceOption.get

        if (curBitmapSource == bitmapSource) {
          //same work is already in progress
          false
        } else {
          //cancel previous task, if returns true means is cancelled and yes create new task
          //if returns false cancel it please and do not
          task.cancel({
            val mayInterruptIfRunning = true

            mayInterruptIfRunning
          })
        }
      } else {
        true
      }
    } else {
      true
    }
  }

  /**
   * @return retrieve the task associated with a particular ImageView
   */
  private def grabBitmapWorkerTask[T, P](imageView: ImageView) = {
    Option(imageView).flatMap {
      imgView =>
        val drawable = imgView.getDrawable

        drawable match {
          case asyncDrawable: AsyncDrawable[T, P] => asyncDrawable.getBitmapWorkerTaskOption
          case _ => None
        }
    }
  }
}
