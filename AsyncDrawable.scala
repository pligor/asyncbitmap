package com.pligor.asyncbitmap

import android.content.res.Resources
import android.graphics.drawable.BitmapDrawable
import scala.ref.WeakReference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */

//http://developer.android.com/training/displaying-bitmaps/process-bitmap.html
class AsyncDrawable[T, P](res: Resources,
                       private val bitmapWorkerTask: BitmapWorkerTask[T, P]) extends BitmapDrawable(res) {
  private val bitmapWorkerTaskReference = new WeakReference(bitmapWorkerTask)

  def getBitmapWorkerTaskOption = bitmapWorkerTaskReference.get
}
