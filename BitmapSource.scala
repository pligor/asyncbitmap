package com.pligor.asyncbitmap

import android.graphics.Bitmap

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait BitmapSource[T, P] {
  //val getBitmapFunc: (_) => ((_) => Bitmap);  in case you wanted a duble function but this is not that generic

  def getBitmapFunc: (P) => Bitmap

  protected def getUniqueId: T

  override def equals(obj: Any): Boolean = {
    //super.equals(o)
    obj match {
      case that: BitmapSource[T, P] => this.getUniqueId == that.getUniqueId
      case _ => false
    }
  }
}

/*class hello extends BitmapSource[Int] {
  def getUniqueId: Int = 0

  val getBitmapFunc = (tuple: (ImageSizeDp, Context)) => {
    getBitmap(tuple._1)(tuple._2);
  };

  def getBitmap(targetSize: ImageSizeDp)(implicit context: Context): Bitmap = {
    null;
  }
}*/
