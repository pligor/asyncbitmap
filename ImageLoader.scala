package com.pligor.asyncbitmap

import android.graphics.Bitmap
import android.widget.ImageView
import android.content.Context
import scala.concurrent.ExecutionContext

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object ImageLoader {
  def loadImage[T, P](bitmapSource: BitmapSource[T, P],
                      imageView: ImageView,
                      param: P,
                      preOp: (ImageView) => Unit = (dummy) => Unit,
                      postOp: (ImageView, Bitmap) => Unit = (imgView, bitmap) => {
                        imgView.setImageBitmap(bitmap)
                      })(implicit context: Context, executionContext: ExecutionContext): Unit = {
    if (AsyncBitmap.cancelPotentialWork(imageView, bitmapSource)) {
      //if image already has a background task, cancel it
      val task = new BitmapWorkerTask[T, P](imageView,
        preOp = preOp,
        backOp = {
          bitmapSource: BitmapSource[T, P] => bitmapSource.getBitmapFunc(param)
        },
        postOp = postOp)

      //CLEVER TRICK: TO TASK TO APOTHIKEUOUME ENTOS TOU DRAWABLE
      val asyncDrawable = new AsyncDrawable[T, P](context.getResources, task)
      //alla mhn ksexname oti kai to task exei reference sto imageview
      //o enas exei reference ston allo diladi :P
      //wste to task na mporei na orisei nea bitmaps sto image view
      //kai to drawable na mporei na kanei cancel to task an auto xreiazetai

      //Pws tha katalavoume oti o enas kai o allos einai ontws zeugari?
      //tha orisoume kati koino kai monadiko, px. to uri

      imageView.setImageDrawable(asyncDrawable)

      //now that are all set up, we can execute task

      task.execute(bitmapSource)
    } else {
      //same work is already in progress
    }
  }
}
