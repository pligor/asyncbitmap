This is a package which helps multiple images to be loaded on the background, and if the views are replaced by new views you will not have any issues since the old background process gets cancelled.

You need the method `loadImage` inside `ImageLoader` object.

Make sure that you have attached the trait `BitmapSource` to the model class that is going to provide the image

###Dependencies###
* com.pligor.myandroid

Used in the following projects:

* [bman](http://bman.co)
* [fcarrier](http://facebook.com/FcarrierApp)