package com.pligor.asyncbitmap

import android.widget.ImageView
import scala.ref.WeakReference
import android.graphics.Bitmap
import com.pligor.myandroid.JavaAsyncTask

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class BitmapWorkerTask[T, P](private val imageView: ImageView,
                             private val preOp: (ImageView) => Unit,
                             private val backOp: (BitmapSource[T, P]) => Bitmap,
                             private val postOp: (ImageView, Bitmap) => Unit)
  extends JavaAsyncTask[BitmapSource[T, P], AnyRef, Bitmap] {

  private val imageViewReference = new WeakReference(imageView)

  private var curBitmapSourceOption: Option[BitmapSource[T, P]] = None

  def getCurBitmapSourceOption = curBitmapSourceOption

  override def onPreExecute(): Unit = {
    super.onPreExecute()

    imageViewReference.get.map(preOp)
  }

  protected def doInBackground(bitmapSource: BitmapSource[T, P]): Bitmap = {
    curBitmapSourceOption = Some(bitmapSource)

    backOp(bitmapSource)
  }

  override def onPostExecute(bitmap: Bitmap): Unit = {
    super.onPostExecute(bitmap)

    val imageViewOption = imageViewReference.get

    if ((!isCancelled) && imageViewOption.isDefined) {
      postOp(imageViewOption.get, bitmap)
    } else {
      //nop at the moment
    }
  }
}
